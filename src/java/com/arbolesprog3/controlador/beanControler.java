/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador;

import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author marthalucia
 */
@Named(value = "beanControler")
@SessionScoped
public class beanControler implements Serializable {
    private String usuario;
    private String contrasena;
    private String rol;

    /**
     * Creates a new instance of beanControler
     */
    public beanControler() {
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
    public String ingresoUsuarios()
    {
        if(usuario.equals("Martha") && contrasena.equals("09876") && rol.equals("Administrador"))
        {
            return "irBatallaNaval";
        } else if(usuario.equals("Valen") && contrasena.equals("Valens02") && rol.equals("Jugador")){
            return "irPrincipal";
        }
        else {
        JsfUtil.addErrorMessage("ingreso incorrecto");
        }
        return null;
    }
}

