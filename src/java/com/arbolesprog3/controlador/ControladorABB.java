/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.modelo.ArbolABB;
import com.arbolesprog3.modelo.Celular;
import com.arbolesprog3.modelo.Marca;
import com.arbolesprog3.modelo.NodoABB;
import com.arbolesprog3.modelo.Operador;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author carloaiza
 */
@Named(value = "controladorABB")
@SessionScoped
public class ControladorABB implements Serializable {

    private boolean verRegistrar = false;
    private DefaultDiagramModel model;
    private List<Marca> marcas;
    private List<Operador> operadores;
    private Celular celular = new Celular();
    private String encontrarImei;

    private ArbolABB arbol = new ArbolABB();

    
    
    

    /**
     * Creates a new instance of ControladorABB
     */
   

    public ControladorABB() {
    }

    public String getEncontrarImei() {
        return encontrarImei;
    }

    public void setEncontrarImei(String encontrarImei) {
        this.encontrarImei = encontrarImei;
    }

    public ArbolABB getArbol() {
        return arbol;
    }

    public void setArbol(ArbolABB arbol) {
        this.arbol = arbol;
    }

    public Celular getCelular() {
        return celular;
    }

    public void setCelular(Celular celular) {
        this.celular = celular;
    }

    public List<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Marca> marcas) {
        this.marcas = marcas;
    }

    public List<Operador> getOperadores() {
        return operadores;
    }

    public void setOperadores(List<Operador> operadores) {
        this.operadores = operadores;
    }

    @PostConstruct
    private void iniciar() {
        llenarMarcas();
        llenarOperadores();
        try {
            arbol.adicionarNodo(new Celular(
                    "123", "3016066906", marcas.get(0), operadores.get(0), "Negro", 250000));
            arbol.adicionarNodo(new Celular(
                    "120", "3016066897", marcas.get(1), operadores.get(1), "Blanco", 330000));
            arbol.adicionarNodo(new Celular(
                    "234", "3016066785", marcas.get(2), operadores.get(1), "Gris", 400000));
            arbol.adicionarNodo(new Celular(
                    "621", "3016063456", marcas.get(0), operadores.get(0), "Negro", 500000));

        } catch (CelularExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
        pintarArbol();

    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public void habilitarRegistrar() {
        verRegistrar = true;
        celular = new Celular();
    }

    public void deshabilitarRegistrar() {
        verRegistrar = false;
    }

    public void pintarArbol() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbol.getRaiz(), model, null, 30, 0);
    }

    private void pintarArbol(NodoABB reco, DefaultDiagramModel model,
            Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");

            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);

            pintarArbol(reco.getIzquierda(), model, elementHijo, x - 5, y + 8);
            pintarArbol(reco.getDerecha(), model, elementHijo, x + 5, y + 8);
        }
    }

    private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");

        return endPoint;
    }

    public DiagramModel getModel() {
        return model;
    }

    private void llenarMarcas() {
        marcas = new ArrayList<>();
        marcas.add(new Marca((short) 1, "Huawei"));
        marcas.add(new Marca((short) 2, "LG"));
        marcas.add(new Marca((short) 3, "Sony"));

    }

    private void llenarOperadores() {
        operadores = new ArrayList<>();
        operadores.add(new Operador("Movistar", "Manizales", "4G"));
        operadores.add(new Operador("Claro", "Pereira", "3G"));

    }

    public void guardarCelular() {
        try {
            arbol.adicionarNodo(celular);
            celular = new Celular();
            verRegistrar = false;
            pintarArbol();
        } catch (CelularExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    public void borrarMenor() {
        arbol.borrarMenor();
        JsfUtil.addSuccessMessage("Imei menor elimado ");
        pintarArbol();
    }

    public void borrarMayor() {
        arbol.borrarMayor();
        JsfUtil.addSuccessMessage("Imei mayor eliminado");
        pintarArbol();
    }
// ahora

    public void borrarCelular() {
        arbol.borrarDato(getEncontrarImei());
        JsfUtil.addSuccessMessage("Imei elimado");
        pintarArbol();
    }

  

    private String recorrido(ArrayList it, String imei) {
        int i = 0;
        String r = imei + "\n";
        while (i < it.size()) {
            r += "\t" + it.get(i).toString() + "\n";
            i++;
        }
        return (r);
    }
    
     public String ramaMayor() {
        ArrayList it = this.arbol.ObtenerRamamayor();
        return (recorrido(it, "Rama(s) con mas valores"));
    }
     
     public String esta(String buscar) {
        boolean siEsta = this.arbol.buscar(getEncontrarImei());
        String r = "El dato:" + "\n";
        r += siEsta ? "Si se encuentra en el arbol" : "No se encuentra en el arbol";

        return (r);
    }
     public String darHojas() {
        ArrayList it = this.arbol.getHojas();
        return (recorrido(it, "Hojas del Arbol"));
    }
     
     public void podarArbol() {
        this.arbol.podar();
        JsfUtil.addSuccessMessage("arbol podado");
        pintarArbol();
    }
}

