/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador;

import com.arbolesprog3.excepcion.BarcoExcepcion;
import com.arbolesprog3.modelo.ArbolABBBatallaN;
import com.arbolesprog3.modelo.Celular;
import com.arbolesprog3.modelo.NodoABB;
import com.arbolesprog3.modelo.NodoABBBatalla;
import com.arbolesprog3.modelo.TipoBarco;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiven
 */
@Named(value = "controladorBatallaNv")
@SessionScoped
public class controladorBatallaN implements Serializable {
    
      private boolean verRegistrar = false;
    private TipoBarco barco = new TipoBarco();
    private ArbolABBBatallaN arbol = new ArbolABBBatallaN();
    private String buscarBarco="";

    /**
     * Creates a new instance of controladorBatallaNv
     */
    public controladorBatallaN() {
    }

    public String getBuscarBarco() {
        return buscarBarco;
    }

    public void setBuscarBarco(String buscarBarco) {
        this.buscarBarco = buscarBarco;
    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public TipoBarco getBarco() {
        return barco;
    }

    public void setBarco(TipoBarco barco) {
        this.barco = barco;
    }

    public ArbolABBBatallaN getArbol() {
        return arbol;
    }

    public void setArbol(ArbolABBBatallaN arbol) {
        this.arbol = arbol;
    }
    
    
    
    public void habilitarRegistrar() {
        verRegistrar = true;
        barco = new TipoBarco();
    }

    public void deshabilitarRegistrar() {
        verRegistrar = false;
    }
     public void guardarBarco()throws ClassCastException, BarcoExcepcion {
         arbol.adicionaTipoBarco(barco);
         barco = new TipoBarco();
         verRegistrar = false;
     } 
     
      public void borrarBarco() {
        arbol.borrarDato(getBuscarBarco());
        JsfUtil.addSuccessMessage("El barco fue eliminado");
 
    }
   
     
}
