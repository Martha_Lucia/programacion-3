/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import com.arbolesprog3.excepcion.BarcoExcepcion;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiven
 */
public class ArbolABBBatallaN {

    private NodoABBBatalla raiz;
    private int cantidadBarcos;

    public ArbolABBBatallaN() {
    }

    public NodoABBBatalla getRaiz() {
        return raiz;
    }

  public boolean esVacio() {
        return raiz == null;
    }
    

    public void setRaiz(NodoABBBatalla raiz) {
        this.raiz = raiz;
    }

    
    public int getCantidadBarcos() {
        return cantidadBarcos;
    }

    public void setCantidadBarcos(int cantidadBarcos) {
        this.cantidadBarcos = cantidadBarcos;
    }

    public void adicionaTipoBarco(TipoBarco dato) throws BarcoExcepcion {

//        TipoBarcoValidador.validarDatos(dato);
        NodoABBBatalla nuevo = new NodoABBBatalla(dato);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            adicionarTipoBarco(nuevo, raiz);
        }
        cantidadBarcos++;
    }

    private void adicionarTipoBarco(NodoABBBatalla nuevo, NodoABBBatalla pivote)
            throws BarcoExcepcion {
        if (nuevo.getDato().getNombre().compareTo(pivote.getDato().getNombre()) == 0) {
            throw new BarcoExcepcion("Ya existe un barco con el nombre "
                    + nuevo.getDato().getNombre());
        } else if (nuevo.getDato().getNombre().compareTo(pivote.getDato().getNombre()) < 0) {
            //Va por la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                adicionarTipoBarco(nuevo, pivote.getIzquierda());
            }
        } else {
            //Va por la derecha
            if (pivote.getDerecha() == null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarTipoBarco(nuevo, pivote.getDerecha());
            }
        }
    }

    
    public List<TipoBarco> recorrerInOrden() {
        List<TipoBarco> listaTipoBarcos = new ArrayList<>();
        recorrerInOrden(raiz, listaTipoBarcos);
        return listaTipoBarcos;
    }

    private void recorrerInOrden(NodoABBBatalla reco, List<TipoBarco> listado) {
        if (reco != null) {
            recorrerInOrden(reco.getIzquierda(), listado);
            listado.add(reco.getDato());
            recorrerInOrden(reco.getDerecha(), listado);
        }
        
    }
    
           private NodoABBBatalla buscarNodoMenor(NodoABBBatalla pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());
        return (pivote);
     }
     private NodoABBBatalla borrarBarco(NodoABBBatalla pivote, String buscarBarco ) {
        if (pivote == null) {
            return null;//<--Dato no encontrado	
        }
        int compara = pivote.getDato().getNombre().compareTo(buscarBarco);
        if (compara > 0) {
            pivote.setIzquierda(borrarBarco(pivote.getIzquierda(), buscarBarco));
        } else if (compara < 0) {
            pivote.setDerecha(borrarBarco(pivote.getDerecha(), buscarBarco));
        } else {
            if (pivote.getIzquierda() != null && pivote.getDerecha() != null) {
                /*
                 *	Buscar el menor de los derechos y lo intercambia por el dato
                 *	que desea borrar. La idea del algoritmo es que el dato a borrar 
                 *	se coloque en una hoja o en un nodo que no tenga una de sus ramas.
                 **/
                NodoABBBatalla cambiar = buscarNodoMenor(pivote.getDerecha());
                 TipoBarco aupivote = cambiar.getDato();
                cambiar.setDato(pivote.getDato());
                pivote.setDato(aupivote);
                pivote.setDerecha(borrarBarco(pivote.getDerecha(),
                        buscarBarco));

            } else {
                pivote = (pivote.getIzquierda() != null) ? pivote.getIzquierda()
                        : pivote.getDerecha();

            }
        }
        return pivote;
    }

    public NodoABBBatalla borrarDato(String encontrarImei) {
        NodoABBBatalla nodoB = borrarBarco(raiz, encontrarImei);
        this.setRaiz(nodoB);
        return nodoB;
    } 
}
