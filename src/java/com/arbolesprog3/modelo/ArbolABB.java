/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.validador.CelularValidador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carloaiza
 */
public class ArbolABB implements Serializable {

    private NodoABB raiz;
    private int cantidadNodos;
    private int hojaCantidad;

    public ArbolABB() {
    }

    public int getHojaCantidad() {
        return hojaCantidad;
    }

    public void setHojaCantidad(int hojaCantidad) {
        this.hojaCantidad = hojaCantidad;
    }

    public NodoABB getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoABB raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    ///Adicionar en el árbol
    public void adicionarNodo(Celular dato) throws CelularExcepcion {

        CelularValidador.validarDatos(dato);
        NodoABB nuevo = new NodoABB(dato);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            adicionarNodo(nuevo, raiz);
        }
        cantidadNodos++;
    }

    private void adicionarNodo(NodoABB nuevo, NodoABB pivote)
            throws CelularExcepcion {
        if (nuevo.getDato().getImei().compareTo(pivote.getDato().getImei()) == 0) {
            throw new CelularExcepcion("Ya existe un celular con el imei "
                    + nuevo.getDato().getImei());
        } else if (nuevo.getDato().getImei().compareTo(pivote.getDato().
                getImei()) < 0) {
            //Va por la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getIzquierda());
            }
        } else {
            //Va por la derecha
            if (pivote.getDerecha() == null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getDerecha());
            }
        }
    }

    public List<Celular> recorrerInOrden() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerInOrden(raiz, listaCelulares);
        return listaCelulares;
    }

    private void recorrerInOrden(NodoABB reco, List<Celular> listado) {
        if (reco != null) {
            recorrerInOrden(reco.getIzquierda(), listado);
            listado.add(reco.getDato());
            recorrerInOrden(reco.getDerecha(), listado);
        }
    }

    public List<Celular> recorrerPreOrden() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerPreOrden(raiz, listaCelulares);
        return listaCelulares;
    }

    private void recorrerPreOrden(NodoABB reco, List<Celular> listado) {
        if (reco != null) {
            listado.add(reco.getDato());
            recorrerPreOrden(reco.getIzquierda(), listado);
            recorrerPreOrden(reco.getDerecha(), listado);
        }
    }

    public List<Celular> recorrerPostOrden() {
        List<Celular> listaCelulares = new ArrayList<>();
        recorrerPostOrden(raiz, listaCelulares);
        return listaCelulares;
    }

    private void recorrerPostOrden(NodoABB reco, List<Celular> listado) {
        if (reco != null) {

            recorrerPostOrden(reco.getIzquierda(), listado);
            recorrerPostOrden(reco.getDerecha(), listado);
            listado.add(reco.getDato());
        }
    }

    public boolean esVacio() {
        return raiz == null;
    }

    private double sumaPrecios;

    public double sumarPrecios() {
        sumaPrecios = 0;
        sumarInOrden(raiz);
        return sumaPrecios;
    }

    private void sumarInOrden(NodoABB reco) {
        if (reco != null) {
            sumarInOrden(reco.getIzquierda());
            sumaPrecios += reco.getDato().getPrecio();
            sumarInOrden(reco.getDerecha());
        }

    }

    public double calcularPromedioPrecios() {
        return sumarPrecios() / (double) cantidadNodos;
    }

    // valor Mayor
    public String mayor() {
        NodoABB pivote = raiz;
        if (raiz != null) {
            while (pivote.getDerecha() != null) {
                pivote = pivote.getDerecha();
            }
        }
        return ("" + pivote.getDato());
    }

    // Menor
    public String menor() {
        NodoABB pivote = raiz;
        if (raiz != null) {
            while (pivote.getIzquierda() != null) {
                pivote = pivote.getIzquierda();
            }
        }
        return ("" + pivote.getDato());
    }

    // cantidad de hojas
    public String cantidadNodosHoja() {
        hojaCantidad = 0;
        cantidadNodosHoja(raiz);
        return "" + hojaCantidad;

    }

    private void cantidadNodosHoja(NodoABB reco) {
        if (reco != null) {
            if (reco.getIzquierda() == null && reco.getDerecha() == null) {
                hojaCantidad++;
            }
            cantidadNodosHoja(reco.getIzquierda());
            cantidadNodosHoja(reco.getDerecha());
        }
    }

    private NodoABB buscarNodoMenor(NodoABB pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());
        return (pivote);
    }

    public Celular buscarCelularMenor() {
        return buscarCelularMenor(raiz);
    }

    private Celular buscarCelularMenor(NodoABB pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());

        return (pivote.getDato());
    }

    //Borrar menor
    public Celular borrarMenor() {
        NodoABB pivote = raiz.getIzquierda();
        if (raiz != null) {
            if (raiz.getIzquierda() == null) {
                raiz = raiz.getDerecha();
            } else {
                NodoABB anterior = raiz;
                pivote = raiz.getIzquierda();
                while (pivote.getIzquierda() != null) {
                    anterior = pivote;
                    pivote = pivote.getIzquierda();
                }

                anterior.setIzquierda(pivote.getDerecha());
            }
        }
        return pivote.getDato();
    }

    //borrar mayor
    public Celular borrarMayor() {
        NodoABB pivote = raiz.getIzquierda();
        if (raiz != null) {
            if (raiz.getDerecha() == null) {
                raiz = raiz.getIzquierda();
            } else {
                NodoABB anterior = raiz;
                pivote = raiz.getDerecha();
                while (pivote.getDerecha() != null) {
                    anterior = pivote;
                    pivote = pivote.getDerecha();
                }

                anterior.setDerecha(pivote.getIzquierda());
            }
        }
        return pivote.getDato();
    }

    private NodoABB borrarCelular(NodoABB pivote, String encontrarImei) {
        if (pivote == null) {
            return null;//<--Dato no encontrado	
        }
        int compara = pivote.getDato().getImei().compareTo(encontrarImei);
        if (compara > 0) {
            pivote.setIzquierda(borrarCelular(pivote.getIzquierda(), encontrarImei));
        } else if (compara < 0) {
            pivote.setDerecha(borrarCelular(pivote.getDerecha(), encontrarImei));
        } else {
            if (pivote.getIzquierda() != null && pivote.getDerecha() != null) {
                /*
                 *	Buscar el menor de los derechos y lo intercambia por el dato
                 *	que desea borrar. La idea del algoritmo es que el dato a borrar 
                 *	se coloque en una hoja o en un nodo que no tenga una de sus ramas.
                 **/
                NodoABB cambiar = buscarNodoMenor(pivote.getDerecha());
                Celular aupivote = cambiar.getDato();
                cambiar.setDato(pivote.getDato());
                pivote.setDato(aupivote);
                pivote.setDerecha(borrarCelular(pivote.getDerecha(),
                        encontrarImei));

            } else {
                pivote = (pivote.getIzquierda() != null) ? pivote.getIzquierda()
                        : pivote.getDerecha();

            }
        }
        return pivote;
    }

    public NodoABB borrarDato(String encontrarImei) {
        NodoABB nodoB = borrarCelular(raiz, encontrarImei);
        this.setRaiz(nodoB);
        return nodoB;
    }

// buscar
    //Buscar
    public boolean buscar(String encontrarImei) {
        return (buscar(this.raiz, encontrarImei));

    }

    private boolean buscar(NodoABB pivote, String encontrarImei) {
        if (pivote == null) {
            return (false);
        }
        int compara = ((Comparable) pivote.getDato().getImei()).compareTo(encontrarImei);
        if (compara > 0) {
            return (buscar(pivote.getIzquierda(), encontrarImei));
        } else if (compara < 0) {
            return (buscar(pivote.getDerecha(), encontrarImei));
        } else {
            return (true);
        }
    }
    //Obtener el numero de ramas
    int numeroRamas = 0;

    public ArrayList<String> ObtenerRamamayor() {
        obtenernumeroRamas(this.raiz, 0);
        return ObtenerRamamayor(this.raiz, 0, "", new ArrayList<String>());
    }

    public void obtenernumeroRamas(NodoABB pivote, int contador) {
        if (pivote != null) {
            contador++;
            obtenernumeroRamas(pivote.getIzquierda(), contador);
            obtenernumeroRamas(pivote.getDerecha(), contador);
        }
        if (contador > this.numeroRamas) {
            this.numeroRamas = contador;
        }
    }

    public ArrayList<String> ObtenerRamamayor(NodoABB pivote, int contador, String dato, ArrayList lista) {
        if (pivote != null) {
            dato += pivote.getDato() + ",";
            contador++;
            lista = ObtenerRamamayor(pivote.getIzquierda(), contador, dato, lista);
            lista = ObtenerRamamayor(pivote.getDerecha(), contador, dato, lista);

            if (contador == this.numeroRamas) {
                lista.add(dato);
            }
        }
        return lista;
    }
    
    public ArrayList getHojas() {
        ArrayList l = new ArrayList();
        getHojas(this.raiz, l);
        return (l);
    }

    // hojas del arbol
    private void getHojas(NodoABB pivote, ArrayList l) {
        if (pivote != null) {
            if (this.esHoja(pivote)) {
                l.add(pivote.getDato());
            }
            getHojas(pivote.getIzquierda(), l);
            getHojas(pivote.getDerecha(), l);
        }
    }
    protected boolean esHoja(NodoABB pivote) {
        return (pivote != null && pivote.getIzquierda()== null && pivote.getDerecha()== null);
        }
    public void podar() {
        podar(this.raiz);
    }

    private void podar(NodoABB pivote) {
        if (pivote == null) {
            return;
        }
        if (this.esHoja(pivote.getIzquierda())) {
            pivote.setIzquierda(null);
        }
        if (this.esHoja(pivote.getDerecha())) {
            pivote.setDerecha(null);
        }
        podar(pivote.getIzquierda());
        podar(pivote.getDerecha());
    }
    
     }

        