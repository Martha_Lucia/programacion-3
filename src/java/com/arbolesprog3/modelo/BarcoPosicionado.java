/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

/**
 *
 * @author Tiven
 */
public class BarcoPosicionado {
    
    private TipoBarco tipoBarco;
    private Coordenada coordenadas [][];
    private byte nroImpactos;
    private Estado estado;
    private int identificador;

    public BarcoPosicionado() {
    }

    public BarcoPosicionado(TipoBarco tipoBarco, Coordenada[][] coordenadas, byte nroImpactos, Estado estado, int identificador) {
        this.tipoBarco = tipoBarco;
        this.coordenadas = coordenadas;
        this.nroImpactos = nroImpactos;
        this.estado = estado;
        this.identificador = identificador;
    }

    public TipoBarco getTipoBarco() {
        return tipoBarco;
    }

    public void setTipoBarco(TipoBarco tipoBarco) {
        this.tipoBarco = tipoBarco;
    }

    public Coordenada[][] getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(Coordenada[][] coordenadas) {
        this.coordenadas = coordenadas;
    }

    public byte getNroImpactos() {
        return nroImpactos;
    }

    public void setNroImpactos(byte nroImpactos) {
        this.nroImpactos = nroImpactos;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    @Override
    public String toString() {
        return "BarcoPosicionado{" + "tipoBarco=" + tipoBarco + ", coordenadas=" + coordenadas + ", nroImpactos=" + nroImpactos + ", estado=" + estado + ", identificador=" + identificador + '}';
    }
    
    
    
    
    
    
    
}
