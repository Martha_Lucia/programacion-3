/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import java.io.Serializable;

/**
 *
 * @author Tiven
 */
public class NodoABBBatalla implements Serializable {

    private TipoBarco dato;
    private NodoABBBatalla izquierda;
    private NodoABBBatalla derecha;

    public NodoABBBatalla(TipoBarco dato) {
        this.dato = dato;
    }

    public TipoBarco getDato() {
        return dato;
    }

    public void setDato(TipoBarco dato) {
        this.dato = dato;
    }

    public NodoABBBatalla getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(NodoABBBatalla izquierda) {
        this.izquierda = izquierda;
    }

    public NodoABBBatalla getDerecha() {
        return derecha;
    }

    public void setDerecha(NodoABBBatalla derecha) {
        this.derecha = derecha;
    }

}
