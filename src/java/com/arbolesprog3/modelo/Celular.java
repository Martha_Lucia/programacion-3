/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import java.io.Serializable;

/**
 *
 * @author carloaiza
 */
public class Celular implements Serializable {
    private String imei;
    private String numeroLinea;
    private Marca marca;
    private Operador operador;
    private String color;
    private double precio;

    public Celular() {
        marca= new Marca();
        operador = new Operador();
    }

    
    
    public Celular(String imei, String numeroLinea, Marca marca, 
            Operador operador, String color, double precio) {
        this.imei = imei;
        this.numeroLinea = numeroLinea;
        this.marca = marca;
        this.operador = operador;
        this.color = color;
        this.precio= precio;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getNumeroLinea() {
        return numeroLinea;
    }

    public void setNumeroLinea(String numeroLinea) {
        this.numeroLinea = numeroLinea;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Operador getOperador() {
        return operador;
    }

    public void setOperador(Operador operador) {
        this.operador = operador;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    
    
    @Override
    public String toString() {
        return imei;
    }
    
    
    
    
}
